---
layout: post
title: "Conference schedule now available"
card: schedule.1313694e.png
---

<p class="lead">The schedule for Everything Open 2025 has been published.</p>


## Sessions and Schedule

We are pleased to announce the [schedule](/schedule/) is now available.

In 2025 we will have over 50 presentations on open technologies topics, covering Linux, open source software, open hardware, community and much more.
It has been encouraging to see the variety of topics proposed by members of the community and we are looking forward to the presentations - there is certainly something for everyone!

Check out the schedule today to start planning your time at the conference.


## Buy your ticket

Tickets to attend Everything Open 2025 are now on sale.
Our early bird discounts are still available, but will be closing very soon.
Get in today to make sure you don't miss out!

Prices and inclusions are available on our [tickets page](/attend/tickets/).

Please remember that all attendees need to abide by our Code of Conduct.
If you have questions please contact the Organising Team via email at [contact@everythingopen.au](mailto:contact@everythingopen.au).


## Volunteer

We are still looking for people to help out during the event.
If you are able to assist with introducing talks to our audience or using audio visual technology to stream and record the presentations, we are keen to hear from you!

For more information, please check out our [volunteers page](/attend/volunteer/), where we have full details of what we need assistance with. 


## Stay in the know

You can keep up to date with all the Everything Open happenings in the following ways: 

* [Mastodon](https://fosstodon.org/@EverythingOpen) - @EverythingOpen@fosstodon.org, hashtag #eo2025
* [LinkedIn](https://www.linkedin.com/showcase/everythingopen/)
* [Facebook](https://www.facebook.com/EverythingOpenConference/)
* [Bluesky](https://bsky.app/profile/everythingopen.au) - @everythingopen.au
* [X](https://x.com/_everythingopen) - @_everythingopen, hashtag #eo2025
* [Mailing list](https://lists.linux.org.au/mailman/listinfo/eo-announce)
