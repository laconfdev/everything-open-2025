---
layout: post
title: "Announcing Everything Open 2025"
card: launch.9604c372.png
---

<p class="lead">Linux Australia is pleased to announce that Everything Open will be be held in Adelaide, Australia from January 20-22 2025.</p>

Everything Open is a grassroots conference with a focus on open technologies, the community that has built up around this movement and the values that it represents.
The presentations will cover a broad range of subject areas including Linux, open source software, open hardware, open data, open government, and open GLAM (galleries, libraries, archives and museums), to name a few.
As we have come to expect, there will be technical deep-dives into specific topics from project contributors, as well as tutorials on building hardware or using a piece of software, not to mention talks covering the inner workings of our communities.

This is the first time the conference, as either linux.conf.au or Everything Open, has returned to Adelaide in 20 years.
With its vibrant maker community, Linux Australia is excited to once again bring a series of world-class presentations to the South Australian capital.
The three day conference at the Adelaide Convention Centre will have presentations on a range of open technologies topics from community members and project leaders.

## Join the Team

As we start preparing in earnest, we need to expand our organising team.
We have a number of roles that are needed to make the conference a success, including but not limited to:

* Communications officer
* Event coordinator
* Treasurer
* Speaker liaison
* Sponsorship coordinator
* Volunteer coordinator

While being based in Adelaide or nearby is beneficial, it is not a requirement for being a part of the team.
We also recognise that you may not have been involved in running a conference before, so we have guides and experienced people to help you along the way.

Interested? We want to hear from you!
Please reach out to us at [contact@everythingopen.au](mailto:contact@everythingopen.au) and let us know you would like to be involved.

As usual we will also open a Call for Volunteers for people to assist during the week of the conference, which will come out later this year.

## Call for Sessions

The Everything Open 2025 Call for Sessions will open in late June 2024.
We encourage you to start thinking about talks and tutorials to present at the conference, ready to submit a proposal once the dashboard opens.

## Sponsorship opportunities

We have a wide range of sponsorship opportunities available.
In addition to sponsoring the conference overall, there are a number of opportunities to contribute towards specific parts of the event.
If you or your organisation is interested in sponsoring Everything Open, please [get in touch](../../sponsors/prospectus/).

## Stay in the know

Subscribe to our announcement mailing list and follow our social channels to be the first to know about ticket sales, keynote speakers and conference highlights.

* [Mailing list](https://lists.linux.org.au/mailman/listinfo/eo-announce)
* [Mastodon](https://fosstodon.org/@EverythingOpen)
* [LinkedIn](https://www.linkedin.com/showcase/everythingopen/)
* [Facebook](https://www.facebook.com/EverythingOpenConference/)
* [Conference website](https://2025.everythingopen.au/)
