---
layout: post
title: "Call for Sessions Open"
card: sessions.0a76d128.png
---

<p class="lead">Submit your session proposals today - the Everything Open 2025 Call for Sessions is now open.</p>

Everything Open is coming to Tarntanya (Adelaide) at the Adelaide Convention Centre from Mon 20 - Wed 22 January 2025.

We are putting everything in place to welcome everyone to the conference. We are ready to open up our Call for Sessions and we would love for you to speak at our conference!

## Call for Sessions

We invite you to submit a session proposal on a topic you are familiar with via our [proposals portal](/programme/proposals/). 
The Call for Sessions will remain open until 11:59pm 22 September 2024 anywhere on earth (AoE).

There will be multiple streams catering for a wide range of interest areas across the many facets of open methodology and technology, including Linux, open source software, open hardware, standards, data, GLAM, formats and documentation, and our communities. 
In keeping with the conference's aim to be inclusive to all community members, presentations can be aimed at any level, ranging from technical deep-dives through to beginner and intermediate level presentations for those who are newer to the subject.

There will be two types of sessions at Everything Open: talks and tutorials. 
Talks will be 45 minutes long on a single topic presented in lecture format. 
Tutorials are interactive and hands-on in nature, presented in classroom format. 
Each accepted session will receive one Professional level ticket to attend the conference.

The Session Selection Committee is looking forward to reading your submissions. 
We would also like to thank them for coming together and volunteering their time to help put this conference together.

## Sponsor Early

As usual, we have a range of sponsorship opportunities available, for the conference overall as well as the ability to contribute towards specific parts of the event. 
We encourage you to sponsor the conference early, to get the maximum promotion during the lead up to the event. 
If you or your organisation is interested in sponsoring Everything Open, please [get in touch](/sponsors/prospectus/).

## Stay in the know

Subscribe to our announcement mailing list and follow our social channels to be the first to know about ticket sales, keynote speakers and conference highlights.

* [Mailing list](https://lists.linux.org.au/mailman/listinfo/eo-announce)
* [Mastodon](https://fosstodon.org/@EverythingOpen)
* [LinkedIn](https://www.linkedin.com/showcase/everythingopen/)
* [Facebook](https://www.facebook.com/EverythingOpenConference/)
* [Conference website](https://2025.everythingopen.au/)
