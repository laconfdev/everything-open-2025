---
layout: post
title: "Keynote: Steph Piper"
card: keynote_steph.d599c543.png
---

<p class="lead">Introducing Steph Piper as a Keynote for Everything Open 2025!</p>

We are pleased to announce Steph Piper will be presenting a keynote at Everything Open in Adelaide.

{% include keynote_image.html url="/media/img/keynotes/steph-piper.jpg" description="Image: Steph Piper (supplied)" %}

## About Steph Piper

Steph Piper is a creative technologist and maker.
She is the Makerspace Manager at UniSQ, based in Toowoomba, Queensland.
She is the author of Skill Seeker and the Skill Trees project, using gamification to level up your skills.
She also creates electronics kits for young girls that are now sold globally through US and UK stockists.
With a background in biofabrication, Steph also teaches classes in 3D printing, Arduino and Hardware development.
For more info, check out [her website](https://www.makerqueen.com.au/).


## Buy your ticket

Tickets to attend Everything Open 2025 are still available.
Prices and inclusions are available on our [tickets page](/attend/tickets/).

Please remember that all attendees need to abide by our Code of Conduct.
If you have questions please contact the Organising Team via email at [contact@everythingopen.au](mailto:contact@everythingopen.au).


## Stay in the know

You can keep up to date with all the Everything Open happenings in the following ways:

* [Mastodon](https://fosstodon.org/@EverythingOpen) - @EverythingOpen@fosstodon.org, hashtag #eo2025
* [LinkedIn](https://www.linkedin.com/showcase/everythingopen/)
* [Facebook](https://www.facebook.com/EverythingOpenConference/)
* [Bluesky](https://bsky.app/profile/everythingopen.au) - @everythingopen.au
* [X](https://x.com/_everythingopen) - @_everythingopen, hashtag #eo2025
* [Mailing list](https://lists.linux.org.au/mailman/listinfo/eo-announce)
