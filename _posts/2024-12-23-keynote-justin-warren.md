---
layout: post
title: "Keynote: Justin Warren"
card: keynote_justin.38792900.png
---

<p class="lead">We are excited to announce Justin Warren will be presenting a keynote at Everything Open 2025!</p>

{% include keynote_image.html url="/media/img/keynotes/jpw.jpg" description="Image: Justin Warren (supplied)" %}


## About Justin Warren

Justin is the founder and principal analyst at PivotNine, a boutique analyst and consulting firm based in Melbourne, Australia. 
He covers enterprise infrastructure, cloud, and information security technologies with a particular focus on open source. 
He has used Linux as his primary desktop environment since 1996.

An IT industry veteran with extensive global experience, Justin has worked with enterprise organisations including ANZ bank, IBM, NetApp, Pure Storage, Telstra, and VMware as well as a variety of Silicon Valley startups including Isovalent, Illumio, and Solo.io. 
His preferred programming language is Python.

Justin holds an MBA from Melbourne Business School, and is a graduate member of the Australian Institute of Company Directors. 


## Buy your ticket

Tickets to attend Everything Open 2025 are still available.
Prices and inclusions are available on our [tickets page](/attend/tickets/).

Please remember that all attendees need to abide by our Code of Conduct.
If you have questions please contact the Organising Team via email at [contact@everythingopen.au](mailto:contact@everythingopen.au).


## Stay in the know

You can keep up to date with all the Everything Open happenings in the following ways:

* [Mastodon](https://fosstodon.org/@EverythingOpen) - @EverythingOpen@fosstodon.org, hashtag #eo2025
* [LinkedIn](https://www.linkedin.com/showcase/everythingopen/)
* [Facebook](https://www.facebook.com/EverythingOpenConference/)
* [Bluesky](https://bsky.app/profile/everythingopen.au) - @everythingopen.au
* [X](https://x.com/_everythingopen) - @_everythingopen, hashtag #eo2025
* [Mailing list](https://lists.linux.org.au/mailman/listinfo/eo-announce)
