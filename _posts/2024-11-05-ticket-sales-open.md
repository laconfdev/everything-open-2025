---
layout: post
title: "Ticket Sales Now Open"
card: tickets.cd13bfe9.png
---

<p class="lead">Registration is now available for Everything Open 2025.</p>

Tickets to Everything Open 2025 are on sale, with early bird discounts available now!

## Buy your ticket

Tickets to attend Everything Open 2025 are now on sale! 
For the first couple of weeks we also have early bird discounts available, while stocks last.

So, join the region's open source, open data, open GLAM, open gov, open science, open hardware and other open practitioners, and power up today!

Prices and inclusions are available on our [tickets page](/attend/tickets/).
Buy your ticket now via your [dashboard](/dashboard/).

Please remember that all attendees need to abide by our Code of Conduct.
If you have questions please contact the Organising Team via email at [contact@everythingopen.au](mailto:contact@everythingopen.au).

## Sessions and Schedule

We would like to thank everyone who submitted a proposal to our Call for Sessions.

Our Session Selection Committee has reviewed all of these and selected over 50 presentations to be held during the conference.

It has been encouraging to see the variety of topics proposed by members of the community and we are looking forward to the presentations - there is certainly something for everyone!

Our schedule will be available very soon but if you have submitted a session please keep an eye out for emails from us.

## Volunteer - share your energy, enthusiasm and expertise

Are you great with people? Have some familiarity with audio visual technologies? 
Confident addressing an audience? 
Able to use chat systems, online document sharing and social media?

We need help with:

* Checking attendees in when they arrive
* Operating AV equipment such as audio gear and cameras
* Directing people around our venue
* Ensuring talks and tutorials run to schedule
* Setting up and packing up the conference

In return for your help we'll provide you with:

* Food - morning tea, lunch and afternoon tea
* A clean T shirt everyday
* If you want one, a letter of reference at the end of the conference

For more information, please check out our [volunteers page](/attend/volunteer/), where we have full details of what we need assistance with. 
We review and approve applications regularly.

## Stay in the know

While Everything Open is reason enough to get to Adelaide in January, we are curating information on things to do. Stay tuned. 

You can keep up to date with all the Everything Open happenings in the following ways: 

* [Mastodon](https://fosstodon.org/@EverythingOpen) - @EverythingOpen@fosstodon.org, hashtag #eo2025
* [LinkedIn](https://www.linkedin.com/showcase/everythingopen/)
* [Facebook](https://www.facebook.com/EverythingOpenConference/)
* [X](https://x.com/_everythingopen) - @_everythingopen, hashtag #eo2025
* [Mailing list](https://lists.linux.org.au/mailman/listinfo/eo-announce)
