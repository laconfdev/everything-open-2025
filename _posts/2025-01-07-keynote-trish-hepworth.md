---
layout: post
title: "Keynote: Trish Hepworth"
card: keynote_trish.ccb722f3.png
---

<p class="lead">We are excited to announce the third keynote for Everything Open 2025: Trish Hepworth.</p>

{% include keynote_image.html url="/media/img/keynotes/trish-hepworth.jpg" description="Image: Trish Hepworth (supplied)" %}

Trish will be presenting about the past and present open library.
Come on a potted (and somewhat selective) journey through the development of the western conceptualisation of libraries, examining the roots of libraries’ love affair with open and their onward journeys.
Thinking about how and why libraries and open came together gives us some tools to consider the future of openness in libraries.
How do libraries work with legacy collections and structures?
Where is the balance between practicality and advocacy?
How do we work with new technologies, and the unknowable future?
What present do we want, and what future are we enabling?


## About Trish Hepworth

Trish Hepworth is a passionate advocate for the power of libraries.
As Deputy CEO of the Australian Library and Information Association (ALIA), Trish leads the Association’s work across policy, advocacy, research, education and professional matters.
Prior to her role at ALIA Trish has held executive positions across charitable, government and corporate organisations in Australia and internationally.

Trish specialises in legal and policy reform, leading successful reform efforts in areas as diverse as copyright reform and alcohol pregnancy warning labels.
Trish is a member of the steering committee for the Attorney-General's Copyright and AI working group, a board member for the Australian Library and Archives Copyright Coalition (ALACC) and the ALIA representative for the Australian Media Literacy Alliance (AMLA) and BooksCreate Australia.


## Buy your ticket

Tickets to attend Everything Open 2025 are still available.
Prices and inclusions are available on our [tickets page](/attend/tickets/).

Please remember that all attendees need to abide by our Code of Conduct.
If you have questions please contact the Organising Team via email at [contact@everythingopen.au](mailto:contact@everythingopen.au).


## Stay in the know

You can keep up to date with all the Everything Open happenings in the following ways:

* [Mastodon](https://fosstodon.org/@EverythingOpen) - @EverythingOpen@fosstodon.org, hashtag #eo2025
* [LinkedIn](https://www.linkedin.com/showcase/everythingopen/)
* [Facebook](https://www.facebook.com/EverythingOpenConference/)
* [Bluesky](https://bsky.app/profile/everythingopen.au) - @everythingopen.au
* [X](https://x.com/_everythingopen) - @_everythingopen, hashtag #eo2025
* [Mailing list](https://lists.linux.org.au/mailman/listinfo/eo-announce)
