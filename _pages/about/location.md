---
layout: page
title: Location
permalink: /about/location/
sponsors: true
---

## The Location

Adelaide will be the host city for Everything Open 2025.

We would like to acknowledge the Kaurna people who are the traditional custodians of this land.

There are many things to explore in and around Adelaide, as well as further afield within South Australia.
We encourage people to explore the region either side of the conference.

## The Venue

The [Adelaide Convention Centre](https://www.adelaidecc.com.au/) (ACC) is located in the centre of Adelaide's riverbank precinct, alongside Karrawirra Pari (River Torrens).
Everything Open will use the Panorama Rooms and City Rooms to host the keynote and breakout sessions during the conference.

<div class="map-responsive">
    <iframe width="425" height="350" src="https://www.openstreetmap.org/export/embed.html?bbox=138.58715415000918%2C-34.924768409195615%2C138.60026478767398%2C-34.91698303067632&amp;layer=mapnik&amp;marker=-34.920875812256114%2C138.59370946884155" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=-34.92088&amp;mlon=138.59371#map=17/-34.92088/138.59371">View Larger Map</a></small>
</div>
