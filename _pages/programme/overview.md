---
layout: page
title: Programme Overview
permalink: /programme/
sponsors: true
---

The conference consists of three days of sessions covering many open technology topics.
As the conference is being held in Adelaide, Australia during summer, the event will be using Australian Central Daylight Time (ACDT - UTC+10.5).

## Conference Overview

<div class="table-responsive">
  <table class="table table-hover" summary="This table provides a summary of the conference days">
    <thead class="table-dark">
      <tr>
        <th>Day</th>
        <th>Start of Day</th>
        <th>Daytime</th>
        <th>Evening</th>
      </tr>
    </thead>
    <tr>
      <th>Monday</th>
      <td>Conference Welcome<br>Keynote</td>
      <td>Talks and Tutorials</td>
      <td></td>
    </tr>
    <tr>
      <th>Tuesday</th>
      <td>Keynote</td>
      <td>Talks and Tutorials</td>
      <td></td>
    </tr>
    <tr>
      <th>Wednesday</th>
      <td>Keynote</td>
      <td>Talks and Tutorials <br> Lightning Talks <br> Conference Close</td>
      <td></td>
    </tr>
  </table>
</div>

## Daily Schedule

_Please note: this is a draft schedule and is subject to change._

{% include schedule.html %}

## Presentation Types

### Welcome Address

The conference organisers start the event by welcoming everyone and letting everyone know the general housekeeping requirements of the conference.

### Keynotes

Each conference morning will begin with a keynote presentation.
We will announce our speakers in the lead up to the conference, so keep an eye out.

### Talks

Talks are presentations on a single topic that run for either 25 minutes or 45 minutes.
These are generally presented in lecture format and form the bulk of the available conference slots.

### Tutorials

Tutorials are 100 minute interactive learning sessions.
Participants will gain some hands on knowledge or experience in a topic.
These are generally presented in a classroom format and are interactive or hands-on in nature.
Some tutorials will require preparation work to be done or some hardware to be purchased prior to their attendance - please check the individual tutorial descriptions for more information.

### Lightning Talks

Lightning Talks are short, informal presentations (3-5 minutes each) that take place on the final afternoon of the conference.
Registrations will take place during the conference.

### Conference Close

The final presentation of the conference will wrap up the event, including thanking our volunteers for their time and efforts.
