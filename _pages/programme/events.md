---
layout: page
title: Events
permalink: /programme/events/
sponsors: true
---

During Everything Open we will be running some events in addition to the talks and tutorials.

<a id="penguin-dinner"></a>
## Penguin Dinner

Tuesday 21 January, 6:00pm

_For Penguin Dinner ticket holders only._

### Overview

* Penguin Dinner starts at 6:00pm at [Adelaide Zoo](https://www.adelaidezoo.com.au/) in the **Attenborough Room**
* You must have a non-zero number next to the Penguin Dinner icon on your badge to attend
* Activities start at 6pm, with food served from 7pm

### How do I get there?

Please make your own way to Adelaide Zoo, which is situated on Frome Road, near the River Torrens.
Entry is via Frome Road (follow the signs) or Plane Tree Drive next to the Adelaide Botanic Gardens.
The Adelaide Zoo has a [Getting Here](https://www.adelaidezoo.com.au/visitor-information/#getting-here) guide that you can follow.

You will have your badge checked on arrival before entering for the dinner. No badge, no entry.

<div class="map-responsive">
    <iframe width="425" height="350" src="https://www.openstreetmap.org/export/embed.html?bbox=138.5983228683472%2C-34.917264548757835%2C138.61624002456668%2C-34.90927609820075&amp;layer=mapnik&amp;marker=-34.91327042065088%2C138.6072814464569" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=-34.913270&amp;mlon=138.607281#map=17/-34.913270/138.607281">View Larger Map</a></small>
</div>

### How to get from the Convention Centre to the Penguin Dinner at the Adelaide Zoo

There are 3 recommended methods of getting to the Penguin Dinner at the Adelaide Zoo, depending on your fitness and mobility.
Of course, you can arrange your own transport using taxi services, Uber-type services, ride or walk.
The first suggestion is for those who want to have minimal walking.

#### Suggestion 1: Take the 98A Free Bus from Stop 1 on the Morphett Street Bridge

The Free bus leaves around 5:05 PM arriving at Stop 2, Frome Road, Zoo Entrance, around 5:45 PM.

Exit the West Entrance, turn Right, and move to the footpath. Walk along the bridge footpath to Stop 1. Wait for the **98A Bus**.
Exit the bus at **Stop 2 Frome Road**, if in doubt ask the Driver for assistance.

Once on the footpath turn to your Left and follow Frome Road to the Pedestrian crossing.
Cross over when safe and enter the Zoo property. Keep to the right of the pathway which will take you around the Zoo buildings to the Zoo Entrance.
Just to the right of the Zoo entrance is the **Sanctuary event space**, enter through the doors and either go up the stairs to the restaurant or use the lift to the left of the doorway.

_The last bus to return from that stop back into the City is at 7:45 PM._ Returning at any time after that you will need to arrange your own transport or walk further.

[Bus journey on Google Maps](https://www.google.com/maps/d/edit?mid=1LIQM9HzY5jnxfaxpLAYCGl4AE0Ftjbc&usp=sharing)


#### Suggestion 2: Take the Tram from City West to the University and walk 1 km

Exit through the West Entrance and turn to the Right. Walk along the building to the Lift above North Terrace. After going down in the Lift turn Right at the North Terrace exit and walk about 3-4 minutes then cross over to the **City West Tram Stop**. Catch **ONLY** the **Botanic Gardens Tram**. Alight at the **University Stop**.

Using the Pedestrian crossing cross over to the Northern footpath, turn Left. Walk along the footpath to **Gate 22** then enter the University Campus. Follow the path to the publicly accessible lift. Select to Level 2. Exit the lift to the right and follow the pathway and roadways through Victoria Drive. Cross over to the Northern Footpath then turn Right. Walk to the lights at Frome Road. Cross over when safe and enter the Botanic Park. Take the angled pathway to your left and walk another 5 minutes to the Zoo Entrance. Just to the right of the Zoo entrance is the **Sanctuary event space**, enter through the doors and either go up the stairs to the restaurant or use the lift to the left of the doorway.

[Tram journey on Google Maps](https://www.google.com/maps/d/edit?mid=1UbR0pKVBStSAMoZMJFGq5KcdqMhKtyA&usp=sharing)

#### Suggestion 3: Exit from the West Entrance, walk along the Riverbank about 2 km.

Exit the Convention Centre, turn to the Left, walk towards the River, down the nearest stairs and follow the path alongside the River Torrens till you come to the University Footbridge, the 3rd bridge you will come to. At that point take the pathway bringing you back up onto the road level. When on the road footpath turn to your Left. Walk to the lights at Frome Road. Cross over when safe and enter the Botanic Park. Take the angled pathway to your left and walk another 5 minutes to the Zoo Entrance. Just to the right of the Zoo entrance is the **Sanctuary event space**, enter through the doors and either go up the stairs to the restaurant or use the lift to the left of the doorway.

[Walking Route on Google Maps](https://www.google.com/maps/d/edit?mid=1f-QebtOYZa16mnRJc8S16kkDkDFt8Wc&usp=sharing)

