---
layout: page
title: Proposals
permalink: /programme/proposals/
sponsors: true
---

## Important Information

 * Call for Sessions Opens: 20 August 2024
 * Call for Sessions Closes: 11:59pm 22 September 2024 [Anywhere on Earth (AoE)](https://en.wikipedia.org/wiki/Anywhere_on_Earth)

## How to submit

The Call for Sessions is now closed.

Thank you to everyone who submitted a proposal! 
Our Session Selection Committee had several difficult decisions to make to come up with our schedule for 2025.

If you would like to review your proposals, you can access this via your [Dashboard](/dashboard/).

## About the conference

Everything Open is a conference where people gather to learn about the entire world of open technologies, directly from the people who contribute.
Many of these contributors give scheduled presentations, but much interaction occurs during the "hallway track" in between and after the formal sessions between all attendees.
Our aim is to create a deeply technical conference where we bring together industry leaders and experts on a wide range of subjects.

Everything Open welcomes submissions from first-time and seasoned speakers, from all free and open technology communities, and all walks of life.
We respect and encourage diversity at our conference.

Deciding what to speak about at Everything Open can be a tough challenge, particularly for new speakers.
If you would like some ideas on how to write a talk and how to submit a proposal, we recommend watching E. Dunham's [You Should Speak](https://www.youtube.com/watch?v=3QIQNcGnXes) talk from linux.conf.au 2018.

## Proposal Types

We are accepting submissions for different types of proposal:

 * Talk (25 or 45 minutes): These are generally presented in lecture format and form the bulk of the available conference slots. We will have a few short talk slots available, which are an ideal way for new speakers to get an introduction to presenting at a conference.
 * Tutorial (100 minutes): These are generally presented in a classroom format. They should be interactive or hands-on in nature. Tutorials are expected to have a specific learning outcome for attendees.

## <a name="proposer-recognition"></a> Proposer Recognition

In recognition of the value that presenters bring to our conference, each accepted proposal will be entitled to:

 * *One* complimentary ticket to attend the conference
 * Optionally, recognition as a Contributor, available at a discounted rate

If your proposal includes more than one presenter or organiser, the additional people will be entitled to:

 * Professional or Contributor registration at a discounted rate

All participants in a presentation must have a valid ticket to the conference or they will not be able to attend.

As a volunteer run non-profit conference, Everything Open does not pay speakers to present at the conference.

## Accessibility

Everything Open aims to be accommodating to everyone who wants to attend or present at the conference.
We recognise that some people face accessibility challenges.
If you have special accessibility requirements, you can provide that information when submitting your proposal so that we can plan to properly accommodate you.

## Code of Conduct

By agreeing to present at or attend the conference you are agreeing to abide by the [terms and conditions](/attend/terms-and-conditions/).
We require all speakers and delegates to have read, understood, and act according to the standards set forth in our [Code of Conduct](/attend/code-of-conduct/).
It is also important to read our [Health Statement](/attend/health-statement/) to understand how we intend provide the safest possible environment to attendees.

## Recording

To increase the number of people that can view your presentation, Everything Open will record your talk and make it publicly available after the event.
We plan to release recordings of every talk at the conference under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International ([CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)) licence.
When submitting your proposal you may note that you do not wish to have your talk released, although we prefer and encourage all presentations to be recorded.

## FAQ

**What is the difference between private and public summary?**

Private Summary is the portion that will be shown to the review team.
Give some more details of the talk and why it should be chosen.

Public Summary is the portion that will be shown to everyone on the website once the schedule is announced.
Provide enough details of the talk to whet the appetite.
