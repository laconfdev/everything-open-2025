---
layout: page
title: Why should your employees attend Everything Open?
permalink: /attend/why-should-employees-attend/
sponsors: true
---

It can be tough for employers to know which conference to send their employees to.
If you're looking for a conference to help develop your team, Everything Open offers a holistic experience for almost anybody working in technology who has an interest in open source.

Everything Open delivers a multi-day immersive experience into everything open source, providing delegates and organisations with a strong return on investment.
This means delegate can bring the most up-to-date industry knowledge back to share with their colleagues.

## Key information

**Dates:** Monday 20 - Wednesday 22 January 2025

**Venue:** Adelaide Convention Centre (ACC), Adelaide, Australia

**Tickets:** Details on pricing can be found at [https://2025.everythingopen.au/attend/tickets/](https://2025.everythingopen.au/attend/tickets/)

## A bit more about Everything Open 2025

Everything Open is a grassroots conference with a focus on open technologies, the community that has built up around this movement and the values that it represents.
The conference draws upon the experience of the many events that have been run by Linux Australia and its subcommittees, starting with CALU (Conference of Australian Linux Users) in 1999, linux.conf.au over the past twenty years, and the Open Source Developers Conference (OSDC).

The presentations cover a broad range of subject areas, including Linux, open source software, open hardware, open data, open government, open GLAM (galleries, libraries, archives and museums), to name a few.

There are technical deep-dives into specific topics from project contributors, as well as tutorials on building hardware or using a piece of software, not to mention talks covering the inner workings of our communities.

## What will delegates experience?

Everything Open is brought to you by a team of people who have vast experience with running conferences for the open technologies communities.

Delegates will experience:
* A selection of talks and tutorials from diverse presenters who are thought leaders in the Free and Open Source Software (FOSS) community
* Three days of networking with like-minded professionals

Talks and tutorials at the conference are highly diverse, giving delegates access to the latest ground-breaking insight in the open technologies communities.
The nature of Everything Open sees talks extend from the highly technical to business-focused case studies, which enable delegates to experience a breadth of knowledge across the technology industry.
Topics at Everything Open include security and privacy, architecture, open hardware and software, community engagement, diversity promotion, legal and ethical management.

Further details of our sessions are available online at [https://2025.everythingopen.au/](https://2025.everythingopen.au/).

## What does your organisation gain?

Organisations who fund delegates can gain competitive advantage through:

* Access to the latest insight in the open source space
* Greater awareness and understanding of both mature and emergent technologies, enhancing delegates' strategic impact when they return to the office
* Greater awareness and understanding of issues affecting organisations and the open technologies communities, such as enhancing diversity, patent and copyright legislation and communicating to different audiences
* Enhanced technical competence and capability
* Building connections in the open source community, which can be shared around the office
* Sending someone to Everything Open is also a great way to recognise and reward achievement against organisational objectives

Sending your employees to Everything Open allows your organisation to engage with the most up-to-date industry knowledge across everything open source.

If you would like any further information, feel free to reach out by emailing [contact@everythingopen.au](mailto:contact@everythingopen.au).
