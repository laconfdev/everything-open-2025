---
layout: page
title: Financial Assistance
permalink: /attend/assistance/
sponsors: true
---

## Do you need financial assistance to attend Everything Open?

We have a budget for providing financial assistance to Everything Open attendees who might otherwise find it difficult to attend.
This program is a key part of the conference's outreach and inclusion efforts.

Anyone can apply for financial assistance to attend the conference.

You can apply for full or partial financial assistance to cover the costs of what you might need to join the conference.

You could include things like:

* Your conference ticket (at the hobbyist or student level)
* Costs for travel and/or accommodation
* Other costs associated with attending, such as a support worker or child care.

Please ask only for what you need to attend the conference.

We will be processing applications regularly until funds are exhausted, so we encourage you to apply as soon as you can for a greater chance of being accepted.

## How to apply

Applications for financial assistance are assessed on a case by case basis by sending your application to [assistance@everythingopen.au](mailto:assistance@everythingopen.au). Please see the [selection criteria](#selection-criteria) below for the information you should include in your application.

If you have any questions about financial assistance please contact us at [assistance@everythingopen.au](mailto:assistance@everythingopen.au).

You should expect to hear back about your application within around a week.

## Selection criteria

In addition to allocating financial aid based on need, we also consider how we can get the most benefit for all attendees and the wider community.

In your application, please mention if you:

* Are helping achieve our goal of bringing together of a diverse set of backgrounds and perspectives
* Are a volunteer or helping in another way to make the conference more awesome
* Are a member or organiser of an open source or related group and would share your Everything Open experience with the community
* Are a teacher or other educator and attending the conference would benefit students
* Are a maintainer or contributor to an open source project
* Identify as being in any under-represented groups

Note that these are factors we consider, but they are not a checklist.
You do not need to have any of the above to be eligible for financial aid.

## Reimbursement process

* If granted assistance, you will still be expected to organise and purchase everything you need yourself.
* You will receive a maximum amount of reimbursement based on your request.
* Once we've granted assistance, you can send us your receipts for reimbursement.
* Reimbursement usually happens within a week, but please contact us at [assistance@everythingopen.au](mailto:assistance@everythingopen.au) if you need faster reimbursement.
