---
layout: page
title: Terms and Conditions
permalink: /attend/terms-and-conditions/
sponsors: true
---

_Last Updated 26 May 2024_

<hr>
<span class="abstract">
    All attendees of Everything Open 2025 agree to be bound by the Terms and Conditions.
</span>
<hr>

## Registration
Registering for the event does not guarantee your ticket until it has been paid for in full.
Please ensure you pay the registration invoice as soon as possible to secure your ticket.
Prior to registering, please familiarise yourself with the following Terms and Conditions and raise any concerns with the conference team at [contact@everythingopen.au](contact@everythingopen.au).

## Security and credit cards
All transactions are processed by [Stripe](https://stripe.com), and card details are sent over TLS.
The facility accepts Mastercard, Visa, and American Express.
All transactions are performed by the event organisers on behalf of Linux Australia.
Cardholder data is not stored by Linux Australia.

## Cancellation policy

COVID-19 has introduced new challenges and uncertainties to the conference environment, particularly for delegates willing to take advantage of Early Bird registration.
In recognition of this, the conference team has worked to minimise any financial impact of making your commitment early, in the event that either you or we are unable to proceed with the conference as planned.

### Standard Cancellation

If you choose to not attend the conference for a non-medical reason, a cancellation fee may apply, depending on when you cancel your registration.

* Standard cancellations made prior to or on 23 December 2024: any registration fee paid will be refunded in full.
* Standard cancellations made after 23 December 2024: Early Bird registrations incur a 10% cancellation fee, which will be deducted from any registration fee paid. Full fee registrations will incur a 20% fee. The balance will be refunded.

### Medical Cancellation

If in doubt, stay home!
Everything Open 2025 will not penalise you for abiding by medical advice.

The health and safety of Conference attendees is our highest priority.
Linux Australia is mindful of the health risks posed by COVID-19, and the importance of following official medical advice with respect to social distancing, practising good hygiene and staying at home when unwell.

Linux Australia requires that any delegate who is feeling unwell or has cold or flu like symptoms, who may have been in contact with someone with COVID-19, who is under any form of quarantine or stay-at-home orders including awaiting a COVID-19 test result, not attend the Conference.

If at any time, on validated medical grounds, you are unable to attend Everything Open 2025 in person, you must cancel your attendance in writing by email to [contact@everythingopen.au](contact@everythingopen.au).
A refund may be provided at the discretion of Linux Australia, however you may be asked to provide additional details to support your cancellation.

* Medical cancellation made prior to 20 January 2025: A full refund will be provided.
* Medical cancellation made during the conference: A pro rata refund will be provided based on the duration not able to be attended.

### Event Cancellation or Postponement

Linux Australia may cancel, postpone or move the Conference to a fully online event due to circumstances beyond its reasonable control; COVID-19 is front of mind here, but this clause also includes but is not limited to "Acts of God", terrorism, war, strikes or industrial action, fire, explosion, inevitable accident, breakdown of property, changes in law, pandemics or other global health crises.

* In the event of a full cancellation of Everything Open 2025 by Linux Australia, you will be entitled to a full refund of the amount of the Conference registration fee received.
* In the event of postponement of the Conference, we will seek your advice on whether you are willing to attend the rescheduled Conference;
 * If you are, Linux Australia will transfer your registration and guarantee the same conditions and equivalent registration entitlements (subject to reasonable modification and availability) at the rescheduled event.  If, at a later date, you choose not to attend the rescheduled Conference, the Terms and Conditions in place for the rescheduled event will apply to your registration fee.
 * If you are not willing to transfer your registration, we will refund you 100% of your registration fee.
* In the event Everything Open 2025 Onsite is not able to proceed as planned and is moved to a fully Online format your registration will be transferred to an Online registration comparable to your Onsite registration type. Any difference in the registration fee will be refunded to you in full. If you choose not to attend the Online Conference, Linux Australia will not be required to refund any part of the Online Conference registration fee.

The maximum liability of Linux Australia under these Terms and Conditions is limited to a refund of the Conference registration fee with no additional liability for travel costs, hotel costs, or any other associated costs.

## Substitutions
You may substitute another person, however you must contact us with this person's details.
If you wish to substitute after 2 December 2024, please note that we will not be able to provide any personalised items.

## Privacy notice
In the course of registering for the event and related events, personal information will be collected about attendees such as their name, contact details, etc.
This information is required to facilitate registration to to the event, for catering requirements, and for organisers or their agents to contact particular attendees as and when required in respect of the event.
Attendees who do not disclose this personal information will be unable to complete registration at the event and will therefore not be able to attend.

Personal information will only be disclosed to Linux Australia, and to Government agencies where organisers believe disclosure is appropriate for legal compliance and law enforcement; to facilitate court proceedings; to enforce our terms and conditions; or to protect the rights, property, or safety of the event, our attendees, or others.
Linux Australia will not sell your personal information to third parties and will not use your personal information to send promotional material from any of our affiliated partners and/or sponsors.

As part of the registration process attendees will be asked if they would like to subscribe to the event Mailing Lists and/or the event Announce Mailing List.
Attendees who subscribe to one or more of these lists will be sent emails from the event organisers and other subscribers to the Mailing Lists.
If at any time, attendees wish to unsubscribe from any of these Mailing Lists, please follow the 'how to unsubscribe' directions on the bottom of any message you receive from these Mailing Lists.

From time to time event organisers update their information and website practices.
Please regularly review this page for the most recent information about the event privacy practices.

All personal information will be kept private and used only for event registration purposes, statistics for future events, and convenience for future event registration.

## Network
The event may provide attendees in some locations with access to a wired and/or wireless network.
The access to this network is a privilege and not an entitlement, and must be used appropriately.
Inappropriate use includes, but is not limited to: unlawful activities, interfering with the equipment or network access of others and not respecting the reasonable expectations of privacy that attendees have for traffic flowing though the network.

Any deliberately malicious activities on either the wired or wireless networks will be grounds for instant dismissal from the conference (without reimbursement).

If any attendees use the network inappropriately, then the event organisers will take any enforcement action they consider appropriate.
Enforcement action includes but is not limited to:

* suspending access to the network
* disconnecting the network permanently
* the alleged offender may be asked to immediately leave the venue and/or will be prohibited from continuing to attend the conference (without reimbursement)
* the incident may be reported to local or Federal police
* any other measure the event organisers see fit

Beware that for security and operational reasons the event team may both monitor and log network traffic.

## Audio visual
Event organisers may provide recordings of talks (audio and/or video) given at the event.
This service is provided on a best-effort basis only.
Any recordings will be released as and when they are ready, which may be some time after the conclusion of the event, and the recordings may be of varying quality.

## Discrimination and anti-social behaviour
Linux Australia is proud to support diverse groups of people in IT, particularly women, and will not tolerate in any fashion any intimidation, harassment, and/or any abusive, discriminatory or derogatory behaviour by any attendees of the event and/or related events.

Examples of these behaviours and measures the event organisers or Linux Australia may take are set out in the [Code of Conduct](/attend/code-of-conduct/).
By registering for and attending a Linux Australia event, you agree to this [Code of Conduct](/attend/code-of-conduct/).

## Media
There are a limited number of Media Passes available to media personnel.
Media Passes are free of charge, and entitle media personnel to attend the event with all the entitlements of a Professional registration.
Please note, due to the limited numbers of Media Passes available, all Media Passes will need to be approved by the event organisers.

Any media attending the event are required to identify themselves as "media" to attendees prior to speaking on the record with any attendees of the event.
It is the responsibility of the media to introduce themselves to the persons they wish to interview and to arrange any interviews with those persons.
The event organisers will not make introductions or arrange interviews on behalf of media.

To apply for a Media Pass, please contact [contact@everythingopen.au](mailto:contact@everythingopen.au).

## Students
Students who register for a student ticket (if available) to attend the event will be required to provide event organisers with proof that they are eligible for registration as a student, such as providing a valid full time student ID card.

## Alcohol
Liquor licensing laws require persons to be aged 18 years and older before they can lawfully drink alcohol in Australia.
The licensed premises used as event venues will not serve alcohol to persons 18 years and under.
Those attendees who look under 25 years old, may be required to show identification such as a Passport (non-Australian driver licences are generally not accepted) to licensed premises proving they are 18 years old or older.

Those attendees who are younger than 18 years old are required to advise event organisers of their age, when they register.
Those attendees may not be able to attend all of the associated social events.

Please consume alcohol responsibly.
Please note, licensed premises in Australia are prohibited from selling or supplying alcohol to intoxicated persons.
Please adhere to local liquor laws which are available in most licensed establishments.

## Smoke-free
All event venues including the social event venues are smoke-free.
If attendees wish to smoke during the event and/or related events, they must do so in signed areas.
Please consider others and refrain from smoking directly outside the venues' entrances.

## Health and safety
If you are attending the event as a speaker, it is your responsibility to ensure that your talk meets the Health and Safety requirements under Australian law.
If you are unsure, please contact the event organisers on [contact@everythingopen.au](mailto:contact@everythingopen.au) to discuss it further.
