---
layout: page
title: Shirts and Swag
permalink: /attend/shirts/
sponsors: true
---

We are pleased to offer a range of shirts and other items through Redbubble so you can get a memento of attending Everything Open 2025.

In past years we have provided shirts as part of the registration, however we often ended up with a lot of unused shirts that went to waste, and were unable to provide as wide a range as desired by delegates.

By moving everything to an online store we can offer a wider range of items (shirts, laptop sleeves, bags, coffee mugs, etc) while minimising waste.

<a href="https://www.redbubble.com/shop/ap/167700398" class="btn btn-outline-primary" role="button">Buy Shirts and Swag</a>
