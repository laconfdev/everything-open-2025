---
layout: page
title: Colophon
permalink: /colophon/
sponsors: true
---

### Website Copy

We stand on the shoulders of giants, both previous organisers of Linux Australia and other events, including but not limited to:
 * [linux.conf.au 2020](https://lca2020.linux.org.au/)
 * [PyCon AU](https://2019.pycon-au.org/)

### Website Images

 * Adelaide Drone Pano Torrens - &copy; 2022 Ardash Muradian. Licensed under [Attribution-ShareAlike (CC BY-SA 2.0 Deed)](https://creativecommons.org/licenses/by-sa/2.0/). Original can be found on [flickr](https://www.flickr.com/photos/ardash/52592937466/).

### Website Design

Brand for linux.conf.au by [Tania Walker](https://www.taniawalker.com)
Adapted for Everything Open by the conference team.

This website is developed using free and open source software.

 * [Jekyll](https://jekyllrb.com/)
 * [Bootstrap](https://getbootstrap.com/)
