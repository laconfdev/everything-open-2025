## Everything Open 2025 website

https://2025.everythingopen.au

## Development

This site uses [jekyll](https://jekyllrb.com/), a Ruby-based static website generator.
To install Jekyll and run the site, use the following commands:

``` shell
gem install jekyll
jekyll serve -w
```

If you have Visual Studio Code available, you can open the Dev Container to simplify your local development.
This will setup a container that has Jekyll installed, ready for you to do your development work while having a web server running in the background to preview the changes.
